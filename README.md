# App Ref. Card 01

Standalone Spring Boot Application

---

## Projektbeschreibung
App Ref. Card 01 is a standalone Spring Boot application that allows users to manage and access reference cards efficiently.

---

## Voraussetzungen
- **Betriebssystem:** Windows, macOS, Linux
- **Framework Infrastruktur:** Java 11, Maven 3.6+
- **Wissen:** Grundkenntnisse in Java und Spring Boot

---

## Installation und Verwendung

### Installation der benötigten Werkzeuge

- Maven Tutorial for Beginners: [Maven Tutorial](https://www.simplilearn.com/tutorials/maven-tutorial)

### Projekt bauen und starten

Die Ausführung der Befehle erfolgt im Projektordner

1. **Builden mit Maven:**
    ```bash
    mvn package
    ```

    Das Projekt wird gebaut und die entsprechende Jar-Datei im Ordner `target` erstellt (Artefakt).

2. **Starten der Anwendung:**
    ```bash
    java -jar target/app-refcard-01-0.0.1-SNAPSHOT.jar
    ```

    Die App kann im Browser unter der URL [http://localhost:8080](http://localhost:8080) betrachtet werden.

### Inbetriebnahme mit Docker Container

1. **Docker-Image bauen:**
    ```bash
    docker build -t app-refcard-01 .
    ```

2. **Docker-Container starten:**
    ```bash
    docker run -p 8080:8080 app-refcard-01
    ```

    Die App kann im Browser unter der URL [http://localhost:8080](http://localhost:8080) betrachtet werden.
